# 21AUGDevops1 AWS Challenge 
## Goal
Our goal was to recreate the provided architecture shown below using methods we had learnt over the past two weeks.
![image](https://drive.google.com/uc?export=view&id=1y1Y7kAjn16ZnwJ2tWNKVhlNA8pAbHlrN)

Firstly, I broke it down into tasks so as to work in an agile way.
From the architecture diagram I knew I was going to:

* Create a VPC
* Create an RDS
* Create at least one EC2 instance
* Make a security groups
* Create 1 public subnet and two private subnets
* Install Nginx
* Fork and clone a repo 
* Install and use Docker

Although I was unsure how to utilise Docker and it was not a part of the MVP, I  wanted to attempt to use it.

# Approach

At first, I was unsure as to whether it would be better to build the project from the outside in (VPC and build inwards) or inside out (EC2 and RDS first). 
I decided to try the inside out first in which I created an EC2 and installed docker but very quickly I realised this may present a problem with matching VPC ID's because naturally the instance would use the only VPC currently availiable; the default. 
However, our task required use to create a custom VPC. I could not seem to find a change a VPC ID on an EC2 without copying to a new security group so in order to mitigate possible problems so early on I decided to stop the instance and build outside in.

## Creating the VPC
I didn't have an elastic IP address so I allocated one as seen below in the screenshot.

![image](https://drive.google.com/uc?export=view&id=1O1xeVFeRjbmCzK8X-NBOyJD73FNb-pqD)

Next I created a VPC with one public subnet and two private subnets as requested in the architecture drawing. My VPC had an IPv4 CIDR block of 10.0.0.0/16. 
My public and both private subnets all had differing IPv4 CIDR Blocks of 10.0.0.0/24,10.0.1.0/24 and 10.0.2.0/24. A
All subnets were within the same region of Ireland but differing availability zones (a,b and c) to protect from failure in an availability zone. 
As mentioned later in this document I would have liked to of implemented more than on EC2, this would have been useful then.
If I had only one private subnet it would not have ensured privacy.
The elastic IP chosen was of course the one I had allocated earlier in the previous screenshot.


![image](https://drive.google.com/uc?export=view&id=1IhXiflwibspe-dbvrF1jRVbcuFZzeIH7)


![image](https://drive.google.com/uc?export=view&id=1CBXksZ-OKvBf6E6UU-mFa4u683zkhjo1)
As you can see above my VPC was successfully created.

Next I created security groups for the pending rds database instance and EC2 it was imperative that this remained private, so best that it had its own security group instead of sharing it with the security group the ec2 would be on.

![image](https://drive.google.com/uc?export=view&id=1Ml2UZybwLT0SBDQp9WcshDdiqIsowh3t)

above: my public security group

![image](https://drive.google.com/uc?export=view&id=1Xkmg6mrQqaIanxG0TuYy7I0g4QvziT6F)

above: my private security group

## Creating Subnet Group
My subnet group would be needed when making my RDS database in order to attach/link the VPC to it.
The availability zones chosen are the same availability zones for the private subnets as are the IPv4 CIDR blocks.
![image](https://drive.google.com/uc?export=view&id=1O1xeVFeRjbmCzK8X-NBOyJD73FNb-pqD)
Above: My subnet group.

## Route Tables
My route tables have rules(routes) within them that direct traffic, from gateway and subnets to their relevant location.
Both route tables have a local route.
![image](https://drive.google.com/uc?export=view&id=1mQz8yqmDYvvWQ4im3k5tjCWN6w7Q16lL)
above: route table for public subnet access from internet gateway and subnet


![image](https://drive.google.com/uc?export=view&id=1qbeIQDG5fRgCZgQNIOTRs_Ikqhf432bt)
above: other route table allows local traffic

## NAT gateway
My NAT gateway allows my database in the private subnet of the VPC to connect to the internet.
As you can see below, my NAT Gateway is linked to my VPC.

![image](https://drive.google.com/uc?export=view&id=1ICfKrpbVp6qTFhl5eVtFKOr8jP4DHdao)

## Create RDS and EC2

### RDS
To create the RDS I used a free tier template and a t2.micro db instance class as I felt it would be sufficient for my memory and storage needs for the task. 
I connected it with the custom VPC and subnet group I made prior and of course the private security group made previously, naturally this would also mean denying public access in the checkbox too.
This can be seen below in my screenshot.
![image](https://drive.google.com/uc?export=view&id=1p5hQyr6wOAMZ9ISYBy71ZCOBafNO_BhW)

### EC2
To create my EC2 I once again chose a t2.micro for the same reasons as I did for the RDS; it was sufficient.My EC2 is linked to my Custom VPC as the network and the public subnet to ensure it will be internet facing.
I  used a keypair I already had. As you can see below we have port 80,22 and port 5000. Port 5000 because it is a flask app, accessible from anywhere. Port 22 is only allowed access from my IP address for improved security.
![image](https://drive.google.com/uc?export=view&id=1IB9F8_IgFsGDt-z4Vs2iwtu5f8Hs34CE)

## SSH
Once this had all been created, my architecture had been built I SSH'ed into my EC2 instance
I decided to use amazon linux 2 EC2 instance instead of Ubuntu because I was less familiar with amazon linux 2 and wanted to get familiar with changing the user to "ec2-user" in the config file and any other commands that may differ.
Once I had cloned the requirements.txt and installed the req I once again installed docker, which proved difficult as I was unaware of the Amazon Linux 2 commands and instead familiar with ubuntu. 
Rebooting the instance as suggested would not give the expected response of being able to connect to the Docker Daemon. 
The ubuntu commands worked up unitl a certain step, at which point I kept getting stuck. 
So after futher research, I learnt how to install docker on a linux instance.


## Nginx
At first because I was having difficulty installing docker I installed NGINX without Docker, and directly onto the instance, however, once I had solved my docker issue I was able to install NGINX via Docker.
This was fairly straightforward.
![image](https://drive.google.com/uc?export=view&id=1jnuiOlgkY-AgFCJ1ZkOJhLAWc2pQrkmt)

Above:nginx installed without Docker


![image](https://drive.google.com/uc?export=view&id=1UVkntRZJ6TsG_otmPl1AH0wjw3KvpqVt)

Above:nginx installed with Docker

![image](https://drive.google.com/uc?export=view&id=1AzvNeDGxsbVQWEdhtX2MZdXsnXsMJIGo)

Above:welcome to nginx!

Now I was ready to create my database connection string and connect to my RDS database.
I created my string and exported mt secret key. However, when going to run the app I kept getting a "table project_aws" does not exist SQL error.
Having never come across this before, I searched through the SQLAlchemy error website provided on the error page for further reading. Unable to solve this, eventually I was made aware that the mysql client needed to be installed.
Again this was fairly straight forward, I had to set up the database, and the commands can be seen below.

![image](https://drive.google.com/uc?export=view&id=1Y51IhftvFPIvEJAUZVFqyCTp9jnavBmO)

Once this had been done I was able to view the application, however as it is best practice to hide the database connection string I decided to do so.

![image](https://drive.google.com/uc?export=view&id=1VvaOYDJyQm927-ZxlJvvDZD5bAnY58uP)

Lastly on my app.py file I had to import the necessary imports like os and get env,
Then after re-exporting the secret key and database URI to fix a "none type" error and hiding the connection string I ran python3 app.py again and was able to view the application as seen below.
At this point I was believe I achieved the required MVP.

During the project I discovered this reachability tool, that showed me whether my route was reachable.
![image](https://drive.google.com/uc?export=view&id=1KbHx61t_q6KqcC-IRyu1Hag--r9vCa3Y)


# Stretch Goal Implementation:
 There are a few ways I would have implemented each stretch goal had time allowed.
 * Security: Firstly, I would enhance the security of my database by changing my password and username so that it would be harder for someone to attack my database.
            Secondly, I may add a User and assign a role to be enable which user has certain permissions. This user would need Multi-factor authentication to access the server.
            Thirdly, to further reduce risks I would make use of AWS features such as CloudTrail to track changes on activity across AWS. 
            Similarly I would use Cloud Watch to ensure there are real time activity logs in one location so it is easier to monitor suspicious activity and be alerted, should anything untoward be identified and thus causing an alarm to triggered.
            Lastly, after doing some research, I would utilise Elastic Load balancing access logs to inspect traffic by examining the IP addresses to analyzing suspicious activity. 
  * Reusability/Repeatability: I would utilise Cloud Formation and use a YAML file as opposed to doing it all via the management console. 
  This is for two reasons, firstly YAML files are transferable across different programming languages and secondly you can copy and paste parts and reuse them in other builds, which essentially increases efficiency, repeatibility and reusability. The stack my YAML file would be created on would have a different CIDR block to my current VPC so that they do not interact with one another.
* High availability:
        I would initialise another EC2 instance incase my current EC2 instance failed, I would also introduce Elastic Load Balancing to distribute traffic across resources that were allocated(instances,etc).


## Areas for improvement
* A better understanding of Docker would have saved quite a large amount of time. As towards the end I understood how to continue to implement it, but ran out of time.
* Becoming more aquainted with reading SQLAlchemy error messages, this would also save time.
* Utilisation of YAML files: how to build a yaml file as opposed to doing it all via the management console.
* Containerisation: containerise the RDS in Docker



## Conclusion 

In conclusion, I am confident that I have been able to build the required architecture. 
I have solidified the way I would naturally now approach another building task and am also very aware of other approaches I would like to become proficient in to fortify my knowledge and become more proficient in the world of AWS. 
This task has enabled me to reflect on my abilities and shortcomings in a logical whilst actionable way. 


